<?php

require('../../config/config.inc.php');
require_once('libraries/B1.php');
require_once('models/orders.php');
require_once('models/clients.php');
require_once('models/items.php');
require_once('models/helper.php');

class B1CronException extends Exception
{

    private $extraData;

    public function __construct($message = "", $extraData = array(), $code = 0, \Exception $previous = null)
    {
        $this->extraData = $extraData;
        parent::__construct($message, $code, $previous);
    }

    public function getExtraData()
    {
        return $this->extraData;
    }

}

class B1Cron extends Module
{

    const TTL = 3600;
    const MAX_ITERATIONS = 300;
    const ORDERS_PER_ITERATION = 300;
    const ORDER_SYNC_THRESHOLD = 10;
    const PLUGIN_NAME = 'Prestashop';

    private $library;

    private function init()
    {
        set_time_limit(self::TTL);
        ini_set('max_execution_time', self::TTL);
    }

    private function validateAccess()
    {
        $cron_key = Configuration::get('b1_cron_key');
        if ($cron_key == null) {
            throw new B1CronException('Fatal error.');
        }

        $get_key = Tools::getValue('key');
        if ($get_key != $cron_key) {
            exit();
        }
    }

    public function __construct()
    {
        $this->init();
        $this->validateAccess();

        $get_id = Tools::getValue('id');
        try {
            $this->library = new B1([
                'apiKey' => Tools::getValue('b1_api_key', Configuration::get('b1_api_key')),
                'privateKey' => Tools::getValue('b1_private_key', Configuration::get('b1_private_key'))
            ]);
        } catch (B1Exception $e) {
            throw new B1CronException('API key is not provided');
        }

        switch ($get_id) {
            case 'products':
                $this->fetchProducts();
                break;
            case 'quantities':
                $this->fetchQuantities();
                break;
            case 'orders':
                $this->syncOrders();
                break;
            default:
                throw new Exception('Bad action ID specified.');
        }
    }

    private function fetchProducts()
    {
        $i = 0;
        $lid = 0;
        $b1_ids = array();
        if (Tools::getValue('b1_products_next_sync', Configuration::get('b1_products_next_sync')) < date('Y-m-d H:i:s') ||
            Tools::getValue('b1_products_next_sync', Configuration::get('b1_products_next_sync')) == ''
        ) {
            if (Tools::getValue('b1_products_next_sync', Configuration::get('b1_products_next_sync')) != '') {
                Configuration::updateValue('b1_products_next_sync', '');
                Configuration::updateValue('b1_products_sync_count', 0);
            }
            do {
                $productsSyncCount = Tools::getValue('b1_products_next_sync', Configuration::get('b1_products_sync_count'));
                ob_start();
                $i++;
                try {
                    $data = $this->library->exec('shop/product/list', array("lid" => $lid, "pluginName" => self::PLUGIN_NAME));
                    if ($data != false) {
                        foreach ($data['data'] as $item) {
                            $b1_ids[] = pSQL($item['id']);
                            ModelB1Items::addItem(pSQL($item['name']), pSQL($item['code']), pSQL($item['id']));
                            if (Tools::getValue('relation_type', Configuration::get('relation_type')) == '1_1') {
                                if ($item['quantity']) {
                                    $row = ModelB1Items::getProductByB1Id(pSQL($item['id']));
                                    StockAvailable::setQuantity($row['id_product'], null, (int)$item['quantity']);
                                }
                            }
                        }
                        if (count($data['data']) == 100) {
                            $lid = $data['data'][99]['id'];
                        } else {
                            $lid = -1;
                        }
                    } else {
                        Configuration::updateValue('b1_products_sync_count', $productsSyncCount + 1);
                        throw new B1CronException('Error getting data from B1.lt');
                    }
                } catch (B1Exception $e) {
                    Configuration::updateValue('b1_products_sync_count', $productsSyncCount + 1);
                    ModelB1Helper::printPre($e->getMessage());
                    ModelB1Helper::printPre($e->getExtraData());
                }
                echo "$i;";
                ob_end_flush();
            } while ($lid != -1 && $i < self::MAX_ITERATIONS && $productsSyncCount < 9);

            if (Tools::getValue('b1_products_sync_count', Configuration::get('b1_products_sync_count')) >= 10) {
                Configuration::updateValue('b1_products_next_sync', date('Y-m-d H:i:s', strtotime('6 hour')));
            }

            if (!empty($b1_ids)) {
                ModelB1Items::removeBadLinkedItems($b1_ids);
                ModelB1Items::removeOldB1tems($b1_ids);
            }
        }
        echo 'OK';
    }

    private function fetchQuantities()
    {
        if (Tools::getValue('relation_type', Configuration::get('relation_type')) == '1_1') {
            echo 'OK';
            exit();
        }
        $i = 0;
        $lid = 0;
        $b1_ids = array();

        if (Tools::getValue('b1_quantities_sync_count', Configuration::get('b1_quantities_sync_count')) >= 10 &&
            Tools::getValue('b1_quantities_next_sync', Configuration::get('b1_quantities_next_sync')) == ''
        ) {
            Configuration::updateValue('b1_quantities_next_sync', date('Y-m-d H:i:s', strtotime('6 hour')));
        }
        if (Tools::getValue('b1_quantities_next_sync', Configuration::get('b1_quantities_next_sync')) < date('Y-m-d H:i:s') ||
            Tools::getValue('b1_quantities_next_sync', Configuration::get('b1_quantities_next_sync')) == ''
        ) {
            if (Tools::getValue('b1_quantities_next_sync', Configuration::get('b1_quantities_next_sync')) != '') {
                Configuration::updateValue('b1_quantities_next_sync', '');
                Configuration::updateValue('b1_quantities_sync_count', 0);
            }
            do {
                $quantitiesSyncCount = Tools::getValue('b1_quantities_sync_count', Configuration::get('b1_quantities_sync_count'));
                ob_start();
                $i++;
                try {
                    $data = $this->library->exec('shop/product/quantity/list', array("lid" => $lid, "pluginName" => self::PLUGIN_NAME));
                    if ($data != false) {
                        foreach ($data['data'] as $item) {
                            $b1_ids[] = pSQL($item['id']);
                            if ($item['quantity']) {
                                $row = ModelB1Items::getProductByB1Id(pSQL($item['id']));
                                StockAvailable::setQuantity($row['id_product'], null, (int)$item['quantity']);
                            }
                        }
                        if (count($data['data']) == 100) {
                            $lid = $data['data'][99]['id'];
                        } else {
                            $lid = -1;
                        }
                    } else {
                        Configuration::updateValue('b1_quantities_sync_count', $quantitiesSyncCount + 1);
                        throw new B1CronException('Error getting data from B1.lt');
                    }
                } catch (B1Exception $e) {
                    Configuration::updateValue('b1_quantities_sync_count', $quantitiesSyncCount + 1);
                    ModelB1Helper::printPre($e->getMessage());
                    ModelB1Helper::printPre($e->getExtraData());
                }
                echo "$i;";
                ob_end_flush();
            } while ($lid != -1 && $i < self::MAX_ITERATIONS && $quantitiesSyncCount < 9);

            if (!empty($b1_ids)) {
                ModelB1Items::removeBadLinkedItems($b1_ids);
            }
        }
        echo 'OK';
    }

    private function syncOrders()
    {
        $id = time();
        try {
            $orders_sync_from = Tools::getValue('orders_sync_from', Configuration::get('orders_sync_from'));
            $order_status_id = Tools::getValue('order_status_id', Configuration::get('order_status_id'));
            if (!$orders_sync_from) {
                throw new B1CronException('Not set orders_sync_from value');
            }
            ModelB1Orders::resetNotSyncB1Orders();
            ModelB1Orders::hideOrdersFromSyncByDate(pSQL($order_status_id), pSQL($orders_sync_from));
            ModelB1Orders::setSyncId($id, self::TTL, self::ORDER_SYNC_THRESHOLD);
            $data_prefix = Tools::getValue('b1_shop_id', Configuration::get('b1_shop_id'));
            if (!$data_prefix) {
                throw new B1CronException('Not set shop_id value');
            }
            $initial_sync = Tools::getValue('b1_initial_sync', Configuration::get('b1_initial_sync'));
            if ($initial_sync === false) {
                throw new B1CronException('Not set initial_sync value');
            }

            $i = 0;
            do {
                ob_start();
                $i++;
                $orders = ModelB1Orders::getOrdersForSync(pSQL($order_status_id), pSQL($orders_sync_from), $id, self::ORDERS_PER_ITERATION, self::ORDER_SYNC_THRESHOLD);
                $processed = 0;
                foreach ($orders as $item) {
                    if ($item['b1_sync_id'] == null) {
                        ModelB1Orders::addB1Order($item['id_order'], $id);
                    }
                    $order_data = $this->generateOrderData($item, $data_prefix, $initial_sync);
                    try {
                        $request = $this->library->exec('shop/order/add', $order_data['data']);
                        if ($request != false) {
                            ModelB1Orders::assignB1Order($request['data']['orderId'], $item['id_order']);
                            if (!isset($order_data['data']['billing']['refid'])) {
                                ModelB1Clients::addClient($request['data']['clientId'], $order_data['customer_id']);
                            }
                        } else {
                            ModelB1Orders::addFailedOrderSync($item['id_order']);
                            throw new B1CronException('Error getting data from B1.lt');
                        }
                    } catch (B1DuplicateException $e) {
                        ModelB1Orders::addFailedOrderSync($item['id_order']);
                        $receivedData = json_decode($e->getExtraData()['received']);
                        ModelB1Orders::assignB1Order($receivedData->data->orderId, $item['id_order']);
                        ModelB1Helper::printPre($e->getMessage());
                        ModelB1Helper::printPre($e->getExtraData());
                        throw new B1CronException('Error syncing order #' . pSQL($item['id_order']) . ' with B1.lt');
                    } catch (B1Exception $e) {
                        ModelB1Orders::addFailedOrderSync($item['id_order']);
                        ModelB1Helper::printPre($e->getMessage());
                        ModelB1Helper::printPre($e->getExtraData());
                        throw new B1CronException('Error syncing order #' . pSQL($item['id_order']) . ' with B1.lt');
                    }
                    $processed++;
                    echo "$i-$processed;";
                }
                ob_end_flush();
            } while ($processed == self::ORDERS_PER_ITERATION && $i < self::MAX_ITERATIONS);
            if ($initial_sync == 0) {
                Configuration::updateValue('b1_initial_sync', 1);
            }
            echo 'OK';
        } catch (B1CronException $e) {
            ModelB1Orders::resetSyncId($id);
            ModelB1Helper::printPre($e->getMessage());
            ModelB1Helper::printPre($e->getTrace());
            ModelB1Helper::printPre($e->getExtraData());
        }
    }

    private function generateOrderData($item, $data_prefix, $initial_sync)
    {
        $tax_rate = Configuration::get('tax_rate');
        $order = new Order($item['id_order']);
        $order_data = array();
        $client = ModelB1Clients::getB1Client($order->id_customer);
        if (!empty($client)) {
            $order_data['billing']['refid'] = $client['b1_client_id'];
        }
        $order_data['prefix'] = $data_prefix;
        $order_data['writeoff'] = $initial_sync;
        $customer = new Customer($order->id_customer);
        $address_billing = new Address($order->id_address_invoice);
        $address_delivery = new Address($order->id_address_delivery);
        $country_billing = new Country($address_billing->id_country);
        $country_delivery = new Country($address_billing->id_country);
        $order_data['pluginName'] = self::PLUGIN_NAME;
        $order_data['orderid'] = $order->id;
        $order_data['orderdate'] = substr($order->date_add, 0, 10);
        $order_data['orderno'] = $order->reference;
        $order_data['currency'] = Currency::getCurrency($order->id_currency)['iso_code'];
        $order_data['discount'] = intval(round($order->total_discounts_tax_incl * 100));
        $order_data['total'] = intval(round($order->total_paid_tax_incl * 100));
        $order_data['orderemail'] = $customer->email;
        $order_data['vatRate'] = intval(round($tax_rate));
        $order_data['shippingamount'] = intval(round($order->total_shipping_tax_incl * 100));
        $order_data['billing']['name'] = $address_billing->company == '' ? trim($address_billing->firstname . ' ' . $address_billing->lastname) : $address_billing->company;
        $order_data['billing']['iscompany'] = $address_billing->company == '' ? 0 : 1;
        $order_data['billing']['address'] = $address_billing->address1;
        $order_data['billing']['city'] = $address_billing->city;
        $order_data['billing']['country'] = $country_billing->iso_code;
        $order_data['delivery']['name'] = $address_delivery->company == '' ? trim($address_delivery->firstname . ' ' . $address_delivery->lastname) : $address_delivery->company;
        $order_data['delivery']['iscompany'] = $address_delivery->company == '' ? 0 : 1;
        $order_data['delivery']['address'] = $address_delivery->address1;
        $order_data['delivery']['city'] = $address_delivery->city;
        $order_data['delivery']['country'] = $country_delivery->iso_code;
        if ($order_data['billing']['name'] == '') {
            $order_data['billing'] = $order_data['delivery'];
        }
        if ($order_data['delivery']['name'] == '') {
            $order_data['delivery'] = $order_data['billing'];
        }
        if (!$order->getProductsDetail()) {
            throw new B1CronException('Not found order_products data, for #' . pSQL((int)$order->id) . ' order.', array('order_data' => $order_data));
        }
        foreach ($order->getProductsDetail() as $key => $product) {
            if (Tools::getValue('relation_type', Configuration::get('relation_type')) == '1_1') {
                $b1_product = ModelB1Items::getLinkedProduct($product['id_product']);
                if (count($b1_product) > 0) {
                    $order_data['items'][$key]['id'] = $b1_product['0']['b1_product_id'];
                }
            }
            $order_data['items'][$key]['name'] = $product['product_name'];
            $order_data['items'][$key]['quantity'] = intval(round($product['product_quantity'], 2) * 100);
            $order_data['items'][$key]['price'] = intval(round($product['unit_price_tax_incl'], 2) * 100);
            $order_data['items'][$key]['sum'] = intval($order_data['items'][$key]['price'] * round($product['product_quantity']));
        }
        return [
            'customer_id' => $order->id_customer,
            'data' => $order_data
        ];
    }
}

new B1Cron;
