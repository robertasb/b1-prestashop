<?php

if (!defined('_PS_VERSION_'))
    exit;

require_once _PS_MODULE_DIR_ . 'b1accounting/libraries/B1.php';
require_once _PS_MODULE_DIR_ . 'b1accounting/models/items.php';
require_once _PS_MODULE_DIR_ . 'b1accounting/models/orders.php';

class B1Accounting extends Module
{

    public function __construct()
    {
        $this->name = 'b1accounting';
        $this->tab = 'administration';
        $this->version = '0.0.1 beta';
        $this->author = 'Robertas Bernotas';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('B1 Accounting module');
        $this->description = $this->l('B1.lt web accounting module');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('b1accounting'))
            $this->warning = $this->l('No name provided');
    }

    public function getContent()
    {
        if (Tools::getValue('ajax') == 'true') {
            $pagination_data = array();
            $pagination_data['draw'] = Tools::getValue('draw');
            $pagination_data['type'] = Tools::getValue('type');
            $pagination_data['start'] = Tools::getValue('start');
            $pagination_data['length'] = Tools::getValue('length');
            ModelB1Items::getUnlinkedItems(pSQL($pagination_data['start']), pSQL($pagination_data['length']));
            if ($pagination_data['type'] == 'pagination_shop') {
                $ajax_sorting_data = ModelB1Items::getUnlinkedItems(pSQL($pagination_data['start']), pSQL($pagination_data['length']));
                $total = ModelB1Items::getUnlinkedItemsCount();
                foreach ($ajax_sorting_data as $item) {
                    if ($item['reference'] != '') {
                        $code = $item['reference'];
                    } else if ($item['ean13'] != '') {
                        $code = $item['ean13'];
                    } else {
                        $code = $item['upc'];
                    }
                    $pagination_data['data'][] = array('name' => $item['name'], 'id' => $item['id_product'], 'code' => $code);
                }
            }
            if ($pagination_data['type'] == 'pagination_b1') {
                $ajax_sorting_data = ModelB1Items::getB1Items($pagination_data['start'], $pagination_data['length'], Configuration::get('relation_type'));
                $total = ModelB1Items::getB1ItemsCountTable(Tools::getValue('relation_type'));
                foreach ($ajax_sorting_data as $item) {
                    $pagination_data['data'][] = array('name' => $item['name'], 'id' => $item['b1_id'], 'code' => $item['code']);
                }
            }
            if ($pagination_data['type'] == 'pagination_linked') {
                $ajax_sorting_data = ModelB1Items::getLinkedItems($pagination_data['start'], $pagination_data['length']);
                $total = ModelB1Items::getLinkedItemsCount();
                foreach ($ajax_sorting_data as $item) {
                    $pagination_data['data'][] = array('name' => $item['product_name'], 'b1_name' => $item['b1_name'], 'id' => $item['shop_product_id'], 'b1_reference_id' => $item['b1_product_id'], 'code' => $item['code']);
                }
            }

            $pagination_data['recordsTotal'] = $total;
            $pagination_data['recordsFiltered'] = $total;

            if ($pagination_data['recordsTotal'] == 0) {
                $pagination_data['data'] = array();
            }
            echo json_encode($pagination_data);
            exit();
        }
        if (pSQL(Tools::getValue('form')) == 'link_product') {
            if (is_array(Tools::getValue('shop_item'))) {
                foreach (Tools::getValue('shop_item') as $item) {
                    ModelB1Items::linkProduct(Tools::getValue('b1_item'), $item);
                }
            } else {
                ModelB1Items::linkProduct(Tools::getValue('b1_item'), Tools::getValue('shop_item'));
            }
            exit();
        }
        if (pSQL(Tools::getValue('form')) == 'unlink_product') {
            ModelB1Items::unlinkProduct(pSQL(Tools::getValue('shop_item')));
            exit();
        }
        if (pSQL(Tools::getValue('form')) == 'reset_all') {
            ModelB1Items::resetItems();
            exit();
        }
        if (pSQL(Tools::getValue('form')) == 'reset_all_orders') {
            ModelB1Orders::resetOrders();
            exit();
        }
        if (Tools::isSubmit('submitModule')) {
            $old_sync_from = Tools::getValue('orders_sync_from', Configuration::get('orders_sync_from'));
            Configuration::updateValue('b1_api_key', Tools::getValue('b1_api_key'));
            Configuration::updateValue('tax_rate', Tools::getValue('tax_rate'));
            Configuration::updateValue('orders_sync_from', Tools::getValue('orders_sync_from'));
            Configuration::updateValue('relation_type', Tools::getValue('relation_type'));
            Configuration::updateValue('order_status_id', Tools::getValue('order_status_id'));
            Configuration::updateValue('b1_private_key', Tools::getValue('b1_private_key'));
            Configuration::updateValue('b1_shop_id', Tools::getValue('b1_shop_id'));
            Configuration::updateValue('b1_cron_key', Tools::getValue('b1_cron_key'));
            if (Tools::getValue('orders_sync_from', Configuration::get('orders_sync_from')) != $old_sync_from) {
                ModelB1Items::resetItems();
            }
            Tools::redirectAdmin('index.php?tab=AdminModules&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'));
        }

        if ($this->getConfigFieldsValues()['relation_type'] == 'more_1') {
            $this->context->smarty->assign('relation_input', "return '<input type=\"checkbox\" name=\"shop_item[]\" value=\"' + data + '\">';");
        } else {
            $this->context->smarty->assign('relation_input', "return '<input type=\"radio\" name=\"shop_item\" value=\"' + data + '\">';");
        }

        $this->context->smarty->assign('b1_stat_items_eshop', ModelB1Items::getShopItemsCount());
        $this->context->smarty->assign('b1_stat_items_b1', ModelB1Items::getB1ItemsCount());
        $this->context->smarty->assign('b1_stat_failed_orders', ModelB1Orders::getFailedOrders());

        $this->context->smarty->assign('settings_contact_email', 'info@b1.lt');
        $this->context->smarty->assign('settings_documentation_url', 'https://www.b1.lt/doc/api');
        $this->context->smarty->assign('settings_help_page_url', 'https://www.b1.lt/help');

        $this->context->smarty->assign('template_linked', _PS_MODULE_DIR_ . '/b1accounting/views/templates/admin/linked_panel.tpl');
        $this->context->smarty->assign('template_unlinked', _PS_MODULE_DIR_ . '/b1accounting/views/templates/admin/unlinked_panel.tpl');
        $this->context->smarty->assign('template_stats', _PS_MODULE_DIR_ . '/b1accounting/views/templates/admin/stats.tpl');

        return $this->renderForm() . $this->display(__FILE__, 'views/templates/admin/content.tpl');
    }

    public function renderForm()
    {
        $states = ModelB1Orders::getOrderStates();
        $states_html = '';
        foreach ($states as $item) {
            $states_html .= $item['id_order_state'] . ' - ' . $item['name'] . '<br>';
        }

        $options = array(
            array(
                'id_option' => '1_1',
                'name' => 'One to one',
            ),
            array(
                'id_option' => 'more_1',
                'name' => 'More to one',
            ),
        );
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Shop unique identifier'),
                        'name' => 'b1_shop_id',
                        'desc' => $this->l('Custom shop unique identifier for shop identification'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Orders sync from'),
                        'name' => 'orders_sync_from',
                        'desc' => $this->l('Date format: Y-m-d. Example: 2016-10-12 '),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Items relations for link:'),
                        'name' => 'relation_type',
                        'options' => array(
                            'query' => $options,
                            'id' => 'id_option',
                            'name' => 'name',
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Confirmed order status ID'),
                        'name' => 'order_status_id',
                        'desc' => $this->l('Please select that ID, which orders will be sync') . '<br>' . $states_html,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('VAT Tax rate'),
                        'name' => 'tax_rate',
                        'desc' => $this->l('If in your shop enabled VAT please select that  tax ID. If not leave empty'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('API key'),
                        'name' => 'b1_api_key',
                        'desc' => $this->l('B1.lt API key'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Private key'),
                        'name' => 'b1_private_key',
                        'desc' => $this->l('B1.lt private API key'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Cron key'),
                        'name' => 'b1_cron_key',
                        'desc' => $this->l('Cron key for security'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Cron url products'),
                        'name' => 'b1_cron_url_products',
                        'desc' => $this->l('Please add this URL to server Cron jobs list or use Cron services (easycron, setcronjob). Recommended update every 4 hours.'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Cron url quantities'),
                        'name' => 'b1_cron_url_quantities',
                        'desc' => $this->l('Please add this URL to server Cron jobs list or use Cron services (easycron, setcronjob). Recommended update every 5 min.'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Cron url orders'),
                        'name' => 'b1_cron_url_orders',
                        'desc' => $this->l('Please add this URL to server Cron jobs list or use Cron services (easycron, setcronjob). Recommended update every 4 hours.'),
                    ),
                    array(
                        'type' => '',
                        'name' => '',
                        'label' => $this->l('Run crons'),
                        'desc' => '<a class="btn btn-primary btn-xs" taget="_blank" href="' . $this->getConfigFieldsValues()['b1_cron_url_products'] . '">' . $this->l('Sync products') . '</a> ' .
                            '<a class="btn btn-primary btn-xs" taget="_blank" href="' . $this->getConfigFieldsValues()['b1_cron_url_quantities'] . '">' . $this->l('Sync quantities') . '</a> ' .
                            '<a class="btn btn-primary btn-xs" taget="_blank" href=" ' . $this->getConfigFieldsValues()['b1_cron_url_orders'] . '">' . $this->l('Sync orders') . '</a>',
                    ),
                    array(
                        'type' => '',
                        'name' => '',
                        'label' => $this->l('Reset commands'),
                        'desc' => '<button onclick="return reset_all()" class="btn btn-primary btn-xs">' . $this->l('Unlink all items') . '</button> ' .
                            '<button onclick="return reset_all_orders()" class="btn btn-primary btn-xs">' . $this->l('Reset all orders') . '</button> ',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        $cron_url = _PS_BASE_URL_ . __PS_BASE_URI__ . 'modules/b1accounting/cron.php';

        return array(
            'b1_api_key' => Tools::getValue('b1_api_key', Configuration::get('b1_api_key')),
            'tax_rate' => Tools::getValue('tax_rate', Configuration::get('tax_rate')),
            'b1_private_key' => Tools::getValue('b1_private_key', Configuration::get('b1_private_key')),
            'orders_sync_from' => Tools::getValue('orders_sync_from', Configuration::get('orders_sync_from')),
            'relation_type' => Tools::getValue('relation_type', Configuration::get('relation_type')),
            'order_status_id' => Tools::getValue('order_status_id', Configuration::get('order_status_id')),
            'b1_shop_id' => Tools::getValue('b1_shop_id', Configuration::get('b1_shop_id')),
            'b1_cron_key' => Tools::getValue('b1_cron_key', Configuration::get('b1_cron_key')),
            'b1_cron_url' => $cron_url,
            'b1_cron_url_products' => $cron_url . '?id=products&key=' . Configuration::get('b1_cron_key'),
            'b1_cron_url_quantities' => $cron_url . '?id=quantities&key=' . Configuration::get('b1_cron_key'),
            'b1_cron_url_orders' => $cron_url . '?id=orders&key=' . Configuration::get('b1_cron_key'),
        );
    }

    public function install()
    {
        Configuration::updateValue('b1_initial_sync', 0);
        Configuration::updateValue('last_sync_order', 0);
        Configuration::updateValue('b1_shop_id', substr(md5(time()), 0, 5));
        Configuration::updateValue('b1_cron_key', md5(time()));
        Configuration::updateValue('orders_sync_from', date('Y-m-d'));
        Configuration::updateValue('b1_products_sync_count', 0);
        Configuration::updateValue('b1_products_next_sync', '');
        Configuration::updateValue('b1_quantities_sync_count', 0);
        Configuration::updateValue('b1_quantities_next_sync', '');
        Db::getInstance()->query('
            CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'b1_clients (
            `id` INTEGER(10) NOT NULL AUTO_INCREMENT,
            `b1_client_id` INTEGER(10) DEFAULT NULL,
            `shop_client_id` INTEGER(10) DEFAULT NULL,
            PRIMARY KEY(`id`))
            ENGINE=' . _MYSQL_ENGINE_ . ' default CHARSET=utf8'
        );
        Db::getInstance()->query('
            CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'b1_linked_products (
            `id` INTEGER(10) NOT NULL AUTO_INCREMENT,
            `b1_product_id` INTEGER(10) DEFAULT NULL,
            `shop_product_id` INTEGER(10) DEFAULT NULL,
            PRIMARY KEY(`id`))
            ENGINE=' . _MYSQL_ENGINE_ . ' default CHARSET=utf8'
        );
        Db::getInstance()->query('
            CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'b1_orders (
            `b1_order_id` int(11) DEFAULT NULL,
            `shop_order_id` int(11) NOT NULL,
            `b1_sync_count` int(11) NOT NULL DEFAULT \'0\',
            `b1_sync_id` int(11) DEFAULT NULL,
            `next_sync` timestamp NULL DEFAULT NULL
            )
            ENGINE=' . _MYSQL_ENGINE_ . ' default CHARSET=utf8'
        );
        Db::getInstance()->query('
        CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'b1_items` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` text,
            `code` text,
            `b1_id` int(11) DEFAULT NULL,
            PRIMARY KEY(`id`))
            ENGINE=' . _MYSQL_ENGINE_ . ' default CHARSET=utf8'
        );
        Db::getInstance()->query('
            ALTER TABLE `' . _DB_PREFIX_ . 'b1_items`
            ADD UNIQUE KEY `b1_id` (`b1_id`)'
        );
        Db::getInstance()->query('
            ALTER TABLE `' . _DB_PREFIX_ . 'b1_orders` ADD UNIQUE(`shop_order_id`)'
        );
        Db::getInstance()->query('
            ALTER TABLE `' . _DB_PREFIX_ . 'b1_orders` ADD UNIQUE(`b1_order_id`)'
        );
        Db::getInstance()->query('
            ALTER TABLE `' . _DB_PREFIX_ . 'b1_clients` ADD UNIQUE(`b1_client_id`)'
        );
        Db::getInstance()->query('
            ALTER TABLE `' . _DB_PREFIX_ . 'b1_clients` ADD UNIQUE(`shop_client_id`)'
        );
        if (!parent::install())
            return false;
        return true;
    }

    public function uninstall()
    {
        Configuration::deleteByName('b1_initial_sync');
        Configuration::deleteByName('last_sync_order');
        Configuration::deleteByName('b1_cron_key');
        Configuration::deleteByName('b1_shop_id');
        Configuration::deleteByName('orders_sync_from');
        Configuration::deleteByName('b1_api_key');
        Configuration::deleteByName('b1_private_key');
        Configuration::deleteByName('order_status_id');
        Configuration::deleteByName('relation_type');
        Configuration::deleteByName('tax_rate');

        Db::getInstance()->execute('
        DROP TABLE ' . _DB_PREFIX_ . 'b1_clients'
        );
        Db::getInstance()->execute('
        DROP TABLE ' . _DB_PREFIX_ . 'b1_linked_products'
        );
        Db::getInstance()->execute('
        DROP TABLE ' . _DB_PREFIX_ . 'b1_orders'
        );
        Db::getInstance()->execute('
        DROP TABLE ' . _DB_PREFIX_ . 'b1_items'
        );

        if (!parent::uninstall())
            return false;
        return true;
    }

}
