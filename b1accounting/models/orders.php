<?php

class ModelB1Orders extends Module
{

    public static function resetNotSyncB1Orders()
    {
        \Db::getInstance()->execute("UPDATE `" . _DB_PREFIX_ . "b1_orders` SET `next_sync` = NULL, `b1_sync_count` = 0  WHERE next_sync < '" . date('Y-m-d H:i:s') . "'");
    }

    public static function getFailedOrders()
    {
        $sql = "SELECT COUNT(*) as count FROM `" . _DB_PREFIX_ . "b1_orders` WHERE b1_sync_id IS NOT NULL ";
        return \Db::getInstance()->getValue($sql);
    }

    public static function getOrderStates()
    {
        $sql = "SELECT * FROM `" . _DB_PREFIX_ . "order_state_lang` WHERE id_lang = " . Configuration::get('PS_LANG_DEFAULT') . " ORDER BY id_order_state";
        return \Db::getInstance()->executeS($sql);
    }

    public static function hideOrdersFromSyncByDate($status, $data)
    {
        \Db::getInstance()->execute("UPDATE " . _DB_PREFIX_ . "b1_orders
            LEFT JOIN " . _DB_PREFIX_ . "orders
            ON shop_order_id = id_order
            SET `b1_order_id` = 0
            WHERE `current_state` = '" . pSQL($status) . "'
                AND `b1_sync_id` IS NULL
                AND `invoice_date` != '0000-00-00 00:00:00'
                AND `invoice_date` < '" . pSQL($data) . "
            '");
    }

    public static function setSyncId($id, $ttl, $threshold)
    {
        \Db::getInstance()->execute("UPDATE " . _DB_PREFIX_ . "b1_orders SET `b1_sync_id` = " . $id . " WHERE ((b1_order_id IS NULL AND b1_sync_id IS NULL) OR ($id - b1_sync_id > " . $ttl . " AND b1_sync_count < " . $threshold . ")) AND (next_sync < '" . date('Y-m-d H:i:s') . "' OR next_sync IS NULL)");
    }

    public static function getOrdersForSync($status, $date, $id, $iteration, $thresold)
    {
        return \Db::getInstance()->executeS("SELECT * FROM " . _DB_PREFIX_ . "orders LEFT JOIN " . _DB_PREFIX_ . "b1_orders ON id_order = shop_order_id  WHERE (`current_state` = '" . pSQL($status) . "' AND `invoice_date` != '0000-00-00 00:00:00' AND `invoice_date` >= '" . pSQL($date) . "') AND ((b1_sync_id = " . $id . ") OR (b1_order_id IS NULL)) AND (b1_sync_count < " . $thresold . " || b1_order_id IS NULL ) ORDER BY invoice_date ASC LIMIT " . $iteration);
    }

    public static function addB1Order($shop_order_id, $sync_id)
    {
        \Db::getInstance()->query("INSERT IGNORE INTO `" . _DB_PREFIX_ . "b1_orders` (`b1_order_id`, `shop_order_id`, `b1_sync_count` , `b1_sync_id` ) VALUES (NULL, '" . $shop_order_id . "', NULL,  '" . $sync_id . "')");
    }

    public static function assignB1Order($b1_order, $shop_order)
    {
        \Db::getInstance()->query("UPDATE `" . _DB_PREFIX_ . "b1_orders` SET `b1_sync_id` = NULL , `b1_order_id` = '" . pSQL((int)$b1_order) . "' WHERE `shop_order_id` = '" . pSQL((int)$shop_order) . "'");
    }

    public static function addFailedOrderSync($id)
    {
        \Db::getInstance()->query("UPDATE `" . _DB_PREFIX_ . "b1_orders` SET `b1_sync_id` = NULL , `b1_sync_count` = b1_sync_count + 1, `next_sync` = IF(b1_sync_count >= '10', '" . date('Y-m-d H:i:s', strtotime('6 hour')) . "' , NULL) WHERE `shop_order_id` = '" . pSQL((int)$id) . "'");
    }

    public static function resetSyncId($id)
    {
        \Db::getInstance()->query("UPDATE " . _DB_PREFIX_ . "b1_orders SET `b1_sync_id` = NULL WHERE `b1_sync_id` = '$id'");
    }

    public static function resetOrders()
    {
        \Db::getInstance()->execute("UPDATE `" . _DB_PREFIX_ . "configuration` SET `value` = '0' WHERE `name` = 'b1_initial_sync'");
        \Db::getInstance()->execute("TRUNCATE TABLE " . _DB_PREFIX_ . "b1_orders");
    }

}
