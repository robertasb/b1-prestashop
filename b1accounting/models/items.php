<?php

class ModelB1Items extends Module
{

    public static function getUnlinkedItems($from, $items)
    {
        $result = \Db::getInstance()->executeS('SELECT * FROM ' . _DB_PREFIX_ . 'product
            JOIN ' . _DB_PREFIX_ . 'product_lang ON ' . _DB_PREFIX_ . 'product.id_product = ' . _DB_PREFIX_ . 'product_lang.id_product
            LEFT JOIN ' . _DB_PREFIX_ . 'b1_linked_products ON ' . _DB_PREFIX_ . 'product.id_product = ' . _DB_PREFIX_ . 'b1_linked_products.shop_product_id
            WHERE b1_product_id IS NULL AND id_lang = '. Configuration::get('PS_LANG_DEFAULT') . '
            ORDER BY `name` ASC
            LIMIT ' . pSQL($from) . ', ' . pSQL($items));
        return $result;
    }

    public static function getUnlinkedItemsCount()
    {
        return \Db::getInstance()->getValue('SELECT COUNT(*) as COUNT FROM ' . _DB_PREFIX_ . 'product
            JOIN ' . _DB_PREFIX_ . 'product_lang ON ' . _DB_PREFIX_ . 'product.id_product = ' . _DB_PREFIX_ . 'product_lang.id_product
            LEFT JOIN ' . _DB_PREFIX_ . 'b1_linked_products ON ' . _DB_PREFIX_ . 'product.id_product = ' . _DB_PREFIX_ . 'b1_linked_products.shop_product_id
            WHERE b1_product_id IS NULL AND id_lang = '. Configuration::get('PS_LANG_DEFAULT') . '');
    }

    public static function getB1Items($from, $items, $relation)
    {
        if ($relation == 'more_1') {
            $sql = "SELECT  DISTINCT b1_id, name, code FROM `" . _DB_PREFIX_ . "b1_items` LEFT JOIN `" . _DB_PREFIX_ . "b1_linked_products` ON b1_id = b1_product_id ORDER BY `name` ASC LIMIT " . pSQL($from) . ", " . pSQL($items);
        } else {
            $sql = "SELECT * FROM `" . _DB_PREFIX_ . "b1_items` LEFT JOIN `" . _DB_PREFIX_ . "b1_linked_products` ON b1_id = b1_product_id WHERE `shop_product_id` IS NULL ORDER BY `name` ASC LIMIT " . pSQL($from) . ", " . pSQL($items);
        }
        return \Db::getInstance()->executeS($sql);
    }

    public static function getB1ItemsCountTable($relation)
    {
        if ($relation == 'more_1') {
            return self::getB1ItemsCount();
        } else {
            $sql = "SELECT COUNT(*) as COUNT FROM `" . _DB_PREFIX_ . "b1_items` LEFT JOIN `" . _DB_PREFIX_ . "b1_linked_products` ON b1_id = b1_product_id WHERE `shop_product_id` IS NULL";
            return \Db::getInstance()->getValue($sql);
        }
    }

    public static function getLinkedItems($from, $items)
    {
        $sql = "SELECT *, " . _DB_PREFIX_ . "product_lang.name as product_name, " . _DB_PREFIX_ . "b1_items.name as b1_name  FROM `" . _DB_PREFIX_ . "b1_linked_products` LEFT JOIN `" . _DB_PREFIX_ . "b1_items` ON b1_id = b1_product_id LEFT JOIN `" . _DB_PREFIX_ . "product_lang` ON id_product = shop_product_id  WHERE id_lang = ". Configuration::get('PS_LANG_DEFAULT') . " ORDER BY " . _DB_PREFIX_ . "b1_items.name ASC LIMIT " . pSQL($from) . ", " . pSQL($items);
        return \Db::getInstance()->executeS($sql);
    }

    public static function getLinkedItemsCount()
    {
        $sql = "SELECT COUNT(*) as count FROM `" . _DB_PREFIX_ . "b1_linked_products` ";
        return \Db::getInstance()->getValue($sql);
    }

    public static function unlinkProduct($product_id)
    {
        Db::getInstance()->delete('b1_linked_products', pSQL($product_id) . ' = shop_product_id');
    }

    public static function getShopItemsCount()
    {
        $sql = "SELECT COUNT(*) as count FROM `" . _DB_PREFIX_ . "product` ";
        return \Db::getInstance()->getValue($sql);
    }

    public static function getB1ItemsCount()
    {
        $sql = "SELECT COUNT(*) as count FROM `" . _DB_PREFIX_ . "b1_items` ";
        return \Db::getInstance()->getValue($sql);
    }

    public static function linkProduct($b1_id, $shop_id)
    {
        if ((pSQL($b1_id) != 0) && (pSQL($shop_id) != 0)) {
            \Db::getInstance()->insert('b1_linked_products', array(
                'b1_product_id' => pSQL($b1_id),
                'shop_product_id' => pSQL($shop_id),
            ));
        }
    }

    public static function resetItems()
    {
        \Db::getInstance()->execute("DELETE FROM " . _DB_PREFIX_ . "b1_linked_products");
    }

    public static function resetProducts()
    {
        \Db::getInstance()->execute("UPDATE `" . _DB_PREFIX_ . "b1_orders` SET `b1_sync_id` = NULL WHERE b1_sync_id = 0");
    }

    public static function addItem($name, $code, $id)
    {
        \Db::getInstance()->execute("INSERT IGNORE INTO `" . _DB_PREFIX_ . "b1_items` SET `name` = '" . pSQL($name) . "', `code` = '" . pSQL($code) . "', `b1_id` = '" . pSQL($id) . "'");
    }

    public static function getProductByB1Id($id)
    {
        $row = \Db::getInstance()->getRow('SELECT * FROM ' . _DB_PREFIX_ . 'product JOIN ' . _DB_PREFIX_ . 'b1_linked_products ON ' . _DB_PREFIX_ . 'product.id_product = ' . _DB_PREFIX_ . 'b1_linked_products.shop_product_id WHERE b1_product_id =' . pSQL($id));
        return $row;
    }

    public static function getLinkedProduct($id)
    {
        return \Db::getInstance()->executeS('SELECT * FROM ' . _DB_PREFIX_ . 'b1_linked_products WHERE shop_product_id =' . pSQL($id));
    }

    public static function removeBadLinkedItems($ids)
    {
        \Db::getInstance()->execute("DELETE FROM  " . _DB_PREFIX_ . "b1_linked_products WHERE b1_product_id NOT IN (" . implode(',', $ids) . ")");
    }

    public static function removeOldB1tems($ids)
    {
        \Db::getInstance()->execute("DELETE FROM  " . _DB_PREFIX_ . "b1_items WHERE b1_id NOT IN (" . implode(',', $ids) . ")");
    }

}
