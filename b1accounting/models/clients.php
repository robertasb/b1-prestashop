<?php

class ModelB1Clients extends Module
{
    public static function addClient($b1_client_id, $shop_client_id)
    {
        \Db::getInstance()->query("INSERT IGNORE INTO `" . _DB_PREFIX_ . "b1_clients` (`b1_client_id`, `shop_client_id`) VALUES ('" . $b1_client_id . "' , '" . $shop_client_id . "')");
    }

    public static function getB1Client($shop_client_id)
    {
        return \Db::getInstance()->getRow("SELECT * FROM `" . _DB_PREFIX_ . "b1_clients` WHERE `shop_client_id` = " . $shop_client_id);
    }

}
