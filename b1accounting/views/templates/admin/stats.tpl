<div class="panel panel-default">
    <div class="panel-heading">
        <i class="icon-dashboard"></i> {l s='Statistics' mod='b1accounting'}
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-8">
                {l s='Items in eshop' mod='b1accounting'}
            </div>
            <div class="col-sm-4 text-right">
                <span class="label label-info" id="items-count-eshop">{$b1_stat_items_eshop}</span>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-8">
                {l s='Items in B1' mod='b1accounting'}
            </div>
            <div class="col-sm-4 text-right">
                <span class="label label-primary" id="items-count-b1">{$b1_stat_items_b1}</span>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm-8">
                {l s='Orders that failed to sync' mod='b1accounting'}
            </div>
            <div class="col-sm-4 text-right">
                <span class="label label-danger" id="items-count-order_fail">{$b1_stat_failed_orders}</span>
            </div>
        </div>
    </div>
</div>