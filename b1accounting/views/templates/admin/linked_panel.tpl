<div class="col-md-12">
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-link"></i> {l s='Linked products' mod='b1accounting'}
        </div>
        <table id="b1-linked_items" class="pagination_linked table table-condensed table-hover" data-source-url="<?php echo $url_source; ?>" data-unlink-url="<?php echo $url_unlink; ?>">
            <thead>
            <tr>
                <th>{l s='ID (E. shop)' mod='b1accounting'}</th>
                <th>{l s='ID (B1)' mod='b1accounting'}</th>
                <th>{l s='Name (E. shop)' mod='b1accounting'}</th>
                <th>{l s='Name (B1)' mod='b1accounting'}</th>
                <th>{l s='Code (B1)' mod='b1accounting'}</th>
                <th></th>
            </tr>
            </thead>
        </table>
        <p class="alert alert-info">
            {l s='"Linked products" lists products that are linked between eshop & B1. To unlink simply find the required product in the table and press "Unlink" button.' mod='b1accounting'}
        </p>
    </div>
</div>
<form method="POST" class="hidden">
    <input name="product_id" id="unlink_product_id" type="text">
    <input name="unlink_product" id="unlink_product" type="submit">
</form>
<script>
    function unlink_product(product_id) {
        $("#unlink_product_id").val(product_id);
        $("#unlink_product").click();
    }
</script>
<script>
    $(document).ready(function () {
        $('#linked').DataTable({
            "pagingType": "numbers",
            "lengthChange": false
        });
        $('#unlinked').DataTable({
            "pagingType": "numbers",
            "lengthChange": false
        });
    });
</script>