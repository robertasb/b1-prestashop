<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<div class="panel">
    <div class="panel-heading">
        <i class="icon-link"></i> {l s='Unlinked products' mod='b1accounting'}
    </div>
    <p class="alert alert-info">
        {l s='"Unlinked products" lists eshop products that are not linked with B1 products. Every new product entered in eshop or B1 should be manually linked here.' mod='b1accounting'}
    </p>

    <form id="link" method="POST">
        <input type="hidden" name="form" value="link_product">
        <div class="row">
            <div class="col-sm-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="icon-unlink"></i> {l s='Eshop items' mod='b1accounting'}
                    </div>
                    <div class="panel-body">
                        <table id="eshop-unlinked_items" class="pagination_shop table table-condensed table-hover" data-source-url="<?php echo $url_eshop_source; ?>">
                            <thead>
                            <tr>
                                <th></th>
                                <th>{l s='Name' mod='b1accounting'}</th>
                                <th>{l s='Code' mod='b1accounting'}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 text-center">
                <button type="submit" style="margin-bottom:15px;" class="btn btn-block btn-success">
                    <i class="icon-link"></i> {l s='Link' mod='b1accounting'}
                </button>
            </div>
            <div class="col-sm-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="icon-unlink"></i> {l s='B1 items' mod='b1accounting'}
                    </div>
                    <div class="panel-body">
                        <table id="b1-unlinked_items" class="table pagination_b1 table-condensed table-hover">
                            <thead>
                            <tr>
                                <th></th>
                                <th>{l s='Name' mod='b1accounting'}</th>
                                <th>{l s='Code' mod='b1accounting'}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<form method="POST" class="hidden">
    <input name="product_id" id="product_id" type="text">
    <input name="b1_product_id" id="b1_product_id" type="text">
    <input name="link_product" id="link_product" type="submit">
</form>
<script>
    function link_product(product_id, b1_product_id) {
        $("#product_id").val(product_id);
        $("#b1_product_id").val(b1_product_id);
        $("#link_product").click();
    }
</script>
<script type="text/javascript">
    var frm = $('#link');
    frm.submit(function (ev) {
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
                shop = $('input[name=shop_item]:checked').val();
                b1 = $('input[name=b1_item]:checked').val();
            }
        });
        ev.preventDefault();
        $('.pagination_shop').DataTable().ajax.reload();
        $('.pagination_b1').DataTable().ajax.reload();
    });
</script>