<div class="panel">
    <div class="container-fluid">
        <div class="pull-right">
            <a href="mailto:{$settings_contact_email}" data-toggle="tooltip" title="{l s='Contact' mod='b1accounting'}" class="btn btn-default">
                <i class="icon-envelope"></i> <span class="hidden-xs">{l s='Contact' mod='b1accounting'}</span>
            </a>
            <a href="{$settings_documentation_url}" data-toggle="tooltip" title="{l s='B1 API documentation' mod='b1accounting'}" target="_blank" class="btn btn-default">
                <i class="icon-book"></i> <span class="hidden-xs">{l s='B1 API documentation' mod='b1accounting'}</span>
            </a>
            <a href="{$settings_help_page_url}" data-toggle="tooltip" title="{l s='B1 Help' mod='b1accounting'}" target="_blank" class="btn btn-default">
                <i class="icon-book"></i> <span class="hidden-xs">{l s='B1 Help' mod='b1accounting'}</span>
            </a>
        </div>
    </div>
    <div class="container-fluid">
        <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px;">
            <li class="active">
                <a href="#unlinked_items" data-toggle="tab">
                    <span class="text-warning"><i class="icon-unlink"></i></span> <span class="text-warning hidden-xs">{l s='Unlinked products' mod='b1accounting'}</span>
                </a>
            </li>
            <li>
                <a href="#linked_items" data-toggle="tab">
                    <span class="text-success"><i class="icon-link"></i></span> <span class="text-success hidden-xs">{l s='Linked products' mod='b1accounting'}</span>
                </a>
            </li>
            <li>
                <a href="#stats" data-toggle="tab"><span class="text-primary">
                        <i class="icon-dashboard"></i></span> <span class="text-primary hidden-xs">{l s='Statistics' mod='b1accounting'}</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="unlinked_items">
                {include file="$template_unlinked"}
            </div>
            <div class="tab-pane" id="linked_items">
                {include file="$template_linked"}
            </div>
            <div class="tab-pane" id="stats">
                {include file="$template_stats"}
            </div>
        </div>
    </div>
</div>
{literal}
<script>
    $(document).ready(function () {
        $('.pagination_shop').DataTable({
            "pagingType": "full_numbers",
            "processing": true,
            "serverSide": true,
            "searching": false,
            "ordering": false,
            {/literal}
            "ajax": "{$smarty.server.REQUEST_URI}&type=pagination_shop&ajax=true",
            {literal}
            "columns": [
                {
                    "data": "id",
                    "render": function (data, type, full, meta) {
                        {/literal}{$relation_input}{literal}
                    }
                },
                {"data": "name"},
                {"data": "code"}
            ]
        });
        $('.pagination_b1').DataTable({
            "pagingType": "full_numbers",
            "processing": true,
            "serverSide": true,
            "searching": false,
            "ordering": false,
            {/literal}
            "ajax": "{$smarty.server.REQUEST_URI}&type=pagination_b1&ajax=true",
            {literal}
            "columns": [
                {
                    "data": "id",
                    "render": function (data, type, full, meta) {
                        return '<input type="radio" name="b1_item" value="' + data + '">';
                    }
                },
                {"data": "name"},
                {"data": "code"}
            ]
        });
        $('.pagination_linked').DataTable({
            "pagingType": "full_numbers",
            "processing": true,
            "serverSide": true,
            "searching": false,
            "ordering": false,
            {/literal}
            "ajax": "{$smarty.server.REQUEST_URI}&type=pagination_linked&ajax=true",
            {literal}
            "columns": [
                {"data": "id"},
                {"data": "b1_reference_id"},
                {"data": "name"},
                {"data": "b1_name"},
                {"data": "code"},
                {
                    "data": "id",
                    "render": function (data, type, full, meta) {
                        return '<form class="unlink" method="POST">' +
                            '<input type="hidden" name="form" value="unlink_product">' +
                            '<input type="hidden" name="shop_item" value="' + data + '">' +
                            '<button onclick="return unlink(' + data + ')" type="submit" class="btn btn-primary btn-xs">{/literal}{l s='Unlink' mod='b1accounting'}{literal}</button>' +
                            '</form>';
                    }
                }
            ]
        });

    });

    function unlink(id) {
        $.ajax({
            type: 'POST',
            data: {shop_item: id, form: 'unlink_product'},
            success: function (data) {
                $('.pagination_linked').DataTable().ajax.reload();
            }
        });
        return false;
    }
    {/literal}
    {literal}
    function reset_all() {
        if (window.confirm("Are you sure?")) {
            $.ajax({
                type: 'POST',
                data: {form: 'reset_all'},
                success: function (data) {
                    $('.pagination_linked').DataTable().ajax.reload();
                }
            });
        }
        return false;
    }
    function reset_all_orders() {
        if (window.confirm("Are you sure?")) {
            $.ajax({
                type: 'POST',
                data: {form: 'reset_all_orders'},
                success: function (data) {
                    $('.pagination_linked').DataTable().ajax.reload();
                }
            });
        }
        return false;
    }
    $(document).on("click", "tr", function (e) {
        $(this).find('input:radio').attr('checked', true);
        var chk = $(this).find('input:checkbox').get(0);
        if (chk !== undefined && e.target != chk) {
            chk.checked = !chk.checked;
        }
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        if (target = '#unlinked_items') {
            $('.pagination_shop').DataTable().ajax.reload();
            $('.pagination_b1').DataTable().ajax.reload();
        }
        if (target = '#linked_items') {
            $('.pagination_linked').DataTable().ajax.reload();
        }
    });
</script>
{/literal}
<style>
    .dataTable {
        width: 100% !important;
    }
</style>